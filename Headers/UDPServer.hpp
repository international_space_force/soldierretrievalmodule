#ifndef UDPENCRYPTEDSERVER_UDPSERVER_HPP
#define UDPENCRYPTEDSERVER_UDPSERVER_HPP

#include <memory.h>
#include <netinet/in.h>
#include <cstdio>
#include <cstdlib>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>

class UDPServer {
private:
    int serverPort;
    std::string serverAddress;

    int udpSocket;
    struct sockaddr_in serverAddrData{};
    struct sockaddr_storage serverStorage{};
    socklen_t addr_size;

    void InitializeServer() {
        udpSocket = socket(PF_INET, SOCK_DGRAM, 0);

        serverAddrData.sin_family = AF_INET;
        serverAddrData.sin_port = htons(serverPort);
        serverAddrData.sin_addr.s_addr = inet_addr(serverAddress.c_str());
        memset(serverAddrData.sin_zero, '\0', sizeof serverAddrData.sin_zero);

        bind(udpSocket, (struct sockaddr*)&serverAddrData, sizeof(serverAddrData));

        addr_size = sizeof(serverStorage);
    }

public:
    UDPServer(int port, std::string address) {
        serverPort = port;
        serverAddress = address;

        InitializeServer();
    }

    void GetDatagram(int& receivedByteCount, char* receivedBuffer, int serverDataLimitSize) {
        receivedByteCount = recvfrom(udpSocket, receivedBuffer, serverDataLimitSize, 0, (struct sockaddr*)&serverStorage, &addr_size);
    }

    void SendDatagram(int sendByteCount, char* sendBuffer) {
        sendto(udpSocket, sendBuffer, sendByteCount, 0, (struct sockaddr*)&serverStorage, addr_size);
    }


};

#endif //UDPENCRYPTEDSERVER_UDPSERVER_HPP
