#ifndef CCSCOMMUNICATIONMODULE_MODULEDESIGNATIONS_HPP
#define CCSCOMMUNICATIONMODULE_MODULEDESIGNATIONS_HPP

// Port designations by module
#define UltrasonicSensorModule      4200
#define PathPlanningModule          4201
#define MotorControlModule          4202
#define SoldierRetrievalModule      4203
#define RoverCommunicationModule    4204
#define CentralExecutiveModule      4205
#define IEDSensorModule             4206
#define CameraControlModule         4207
#define GUIModule                   4208
#define CCSCommunicationModule      4209
#define MissionExecutiveModule      4210

// MAVLink system/component ID by module designations
// SYS ID   == source
// COMP ID  == destination
enum MAVLinkMsgModule {
    UltrasonicSensor,
    PathPlanning,
    MotorControl,
    SoldierRetrieval,
    RoverCommunication,
    CentralExecutive,
    IEDSensor,
    CameraControl,
    GUI,
    CCSCommunication,
    MissionExecutive,
};

#endif //CCSCOMMUNICATIONMODULE_MODULEDESIGNATIONS_HPP
