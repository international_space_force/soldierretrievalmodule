#include <cstdio>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <signal.h>
#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPClient.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "MAVLink/common/mavlink.h"
#include "MAVLink/common/mavlink_msg_heartbeat.h"
#include "Headers/TCPClient.hpp"

extern "C" {
#include <wiringPi.h>
}

#include <pigpiod_if2.h>
#include <pigpio.h>

const int DATA_LIMIT_SIZE = 3000;
const std::string LOOPBACK_ADDRESS = "127.0.0.1";
const std::string KEY = "InternationalSpaceForce";
std::mutex driver_mtx;

#define SERVO_1		19	// CH1 -> (Right Servo from behind)
#define SERVO_2		16	// CH2 -> (Left Servo from behind)
#define SERVO_3		17	// CH3 -> (Base)
#define VAC_1		23	// CH4 -> (Vacuum 1)
#define VAC_2		24	// CH4 -> (Vacuum 2)

#define BASE_SERVO_MID	1420
#define MAX_BASE_TURN	40

// TURN_RATE_1: use this to speed up or slow down the turn rate for the base servo
#define TURN_RATE_1		0.5

int pi;

void sendPWMSignals(mavlink_rc_channels_raw_t rc_channels)
{
	int error_stat = 0;
	
	int base_servo = (int)(BASE_SERVO_MID + MAX_BASE_TURN*(rc_channels.chan3_raw - 1500)/500);
	

	printf("Sending PWM Signals...\nCH1: %d CH2: %d CH3: %d CH4: %d\n", rc_channels.chan1_raw, rc_channels.chan2_raw, base_servo, rc_channels.chan4_raw);

	error_stat |= set_servo_pulsewidth(pi, SERVO_1, rc_channels.chan1_raw);

	error_stat |= set_servo_pulsewidth(pi, SERVO_2, rc_channels.chan2_raw);

	error_stat |= set_servo_pulsewidth(pi, SERVO_3, base_servo);

	if(( 1500 < rc_channels.chan5_raw) && (1700 < rc_channels.chan6_raw))
	{
        error_stat |= gpio_write(pi, VAC_1, 1);

        error_stat |= gpio_write(pi, VAC_2, 0);
	}
	else if ((1500 < rc_channels.chan5_raw) && (1300 < rc_channels.chan6_raw) && (rc_channels.chan6_raw < 1700))
	{
        error_stat |= gpio_write(pi, VAC_1, 0);

        error_stat |= gpio_write(pi, VAC_2, 1);
	}
	else
	{
		// turn both of the pumps off
        error_stat |= gpio_write(pi, VAC_1, 0);

        error_stat |= gpio_write(pi, VAC_2, 0);
	}

	if(error_stat != 0)
		printf("Error: %d", error_stat);
}


void PrintToScreen(std::string dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%s", dataToPrint.c_str());
}

void PrintToScreen02(char dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%02X ", dataToPrint);
}

void PrintToScreenChar(char dataToPrint) {
    std::lock_guard<std::mutex>lck(driver_mtx);
    printf("%c ", dataToPrint);
}

std::vector<char> DecryptData(std::vector<char> encryptedData, std::string key) {
    std::vector<char> decyptedData({});

    for(int index = 0; index < encryptedData.size(); index++) {
        if (encryptedData[index] != '\n') {
            int keyModuloIndex = index % key.size();
            decyptedData.push_back((char) encryptedData[index] ^ key[keyModuloIndex]);
        }
        else
            decyptedData.push_back(encryptedData[index]);
    }

    return decyptedData;
}

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorToCharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATA_LIMIT_SIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

[[noreturn]] void SendDataToServerConnection(UDPClient& clientConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        std::vector<char> dataToSend;
        char buffer[DATA_LIMIT_SIZE];

        dataQueue.pop(dataToSend);
        VectorToCharArray(dataToSend, buffer);

        if (!clientConnection.SentDatagramToServerSuccessfully(buffer, dataToSend.size())) {
            printf("Error sending data\n");
        }
    }
}

[[noreturn]] void WaitForDataToBeReceived(UDPServer& serverConnection,
        ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
	std::vector<char> dataToSend;
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void ParseMessagesInQueue(ConcurrentQueue<std::vector<char>>& messagesToParse) {

    while(true) {
	char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer);

        mavlink_message_t msg;
        mavlink_status_t status;
        mavlink_heartbeat_t heartbeat;
        mavlink_rc_channels_raw_t rc_channels;
        unsigned int firstCharInBuffer = -1;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char) firstCharInBuffer);

            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {

		printf("Message ID: %d\n", msg.msgid);

		switch(msg.msgid){

		case 35:
			mavlink_msg_rc_channels_raw_decode(&msg, &rc_channels);

			printf("CH1: %d, CH2: %d CH3: %d CH4: %d, CH5: %d, CH6: %d, CH7: %d, CH8: %d\n", rc_channels.chan1_raw, rc_channels.chan2_raw, rc_channels.chan3_raw, rc_channels.chan4_raw, rc_channels.chan5_raw, rc_channels.chan6_raw, rc_channels.chan7_raw, rc_channels.chan8_raw);

			if(rc_channels.rssi < 200)
			{
				printf("tis the end dear friend\n");
				abort();
			}

			sendPWMSignals(rc_channels);

		default:
			break;		

		}

/*                mavlink_msg_heartbeat_decode(&msg, &heartbeat);
                switch (msg.compid) {
                    case GUI:
                        messagesToGuiModule.push(dataToBeSent);
                        break;

                    case MissionExecutive:
                        messagesToMissionExecutiveModule.push(dataToBeSent);
                        break;

                    default:
                        // Outgoing External Interface to Rover
                        outGoingExternalInterfaceMsgs.push(dataToBeSent);
                        break;
                }*/

            }
        }
        printf("\n");
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        dataQueue.push(dataToSend);

        // Sleep for 10 seconds
        sleep(10);
    }
}

// SendRawRCChannelValues
// Use this if you need to send canned RC Channel values
[[noreturn]] void SendRawRCChannelValues(ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true){

	uint8_t buf[DATA_LIMIT_SIZE];
	mavlink_message_t msg;
	uint16_t len;
	mavlink_msg_rc_channels_raw_pack(SoldierRetrievalModule, SoldierRetrievalModule, &msg, 0, 0, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 255);
	len = mavlink_msg_to_send_buffer(buf, &msg);

	std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
	//dataQueue.push(dataToSend);

	// Sleep for 5 seconds
	sleep(5);
    }
}

void before_exit(int signum)
{
	pigpio_stop(pi);
	printf("Detaching arm gpio!\n");

	exit(signum);
}


int main()
{
	signal(SIGINT, before_exit);

    UDPClient client(MissionExecutiveModule, "127.0.0.1");
    UDPServer receiverFromServerModule(SoldierRetrievalModule, "127.0.0.1");

    ConcurrentQueue<std::vector<char>> sendDataQueue;
    ConcurrentQueue<std::vector<char>> receivedDataQueue;

    char loopControl = '\n';
    //do {
        //PrintToScreen("Hit Enter to continue: ");
        //loopControl = getchar();
    //} while(loopControl == '\n');

    printf("Wiring Pi: %d\n", wiringPiSetup());

    std::thread parseDataReceived(ParseMessagesInQueue, std::ref(receivedDataQueue));
    std::thread sendData(SendDataToServerConnection, std::ref(client), std::ref(sendDataQueue));
    std::thread receiveData(WaitForDataToBeReceived, std::ref(receiverFromServerModule), std::ref(receivedDataQueue));
    std::thread sendHeartbeatStatus(SendHeartbeatStatusMessage, std::ref(sendDataQueue));
    std::thread sendRawRCChannelValues(SendRawRCChannelValues, std::ref(sendDataQueue));

    pi = pigpio_start(NULL, NULL);

    if (pi < 0)
    {
    	fprintf(stderr, "Error starting pigpio! %d\n", pi);

    	printf("skipping over initializing gpio pins\n");
    }
    else
    {
    	printf("pigpio initialization success!");

        printf("setting 23 output: %d\n", set_mode(pi, VAC_1, PI_OUTPUT));

        printf("setting 24 output: %d\n", set_mode(pi, VAC_2, PI_OUTPUT));

        printf("setting 23 LOW: %d\n", gpio_write(pi, VAC_1, 0));

        printf("setting 24 LOW: %d\n", gpio_write(pi, VAC_2, 0));
    }




    PrintToScreen("Outside\n");
    sendData.join();
    sendHeartbeatStatus.join();
    receiveData.join();
    sendRawRCChannelValues.join();



    return 0;
}

