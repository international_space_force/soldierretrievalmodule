// MESSAGE GIMBAL_MANAGER_STATUS support class

#pragma once

namespace mavlink {
namespace common {
namespace msg {

/**
 * @brief GIMBAL_MANAGER_STATUS message
 *
 * Current status about a high level gimbal manager. This message should be broadcast at a low regular rate (e.g. 5Hz).
 */
struct GIMBAL_MANAGER_STATUS : mavlink::Message {
    static constexpr msgid_t MSG_ID = 281;
    static constexpr size_t LENGTH = 8;
    static constexpr size_t MIN_LENGTH = 8;
    static constexpr uint8_t CRC_EXTRA = 252;
    static constexpr auto NAME = "GIMBAL_MANAGER_STATUS";


    uint32_t time_boot_ms; /*< [ms] Timestamp (time since system boot). */
    uint32_t flags; /*<  High level gimbal manager flags currently applied. */


    inline std::string get_name(void) const override
    {
            return NAME;
    }

    inline Info get_message_info(void) const override
    {
            return { MSG_ID, LENGTH, MIN_LENGTH, CRC_EXTRA };
    }

    inline std::string to_yaml(void) const override
    {
        std::stringstream ss;

        ss << NAME << ":" << std::endl;
        ss << "  time_boot_ms: " << time_boot_ms << std::endl;
        ss << "  flags: " << flags << std::endl;

        return ss.str();
    }

    inline void serialize(mavlink::MsgMap &map) const override
    {
        map.reset(MSG_ID, LENGTH);

        map << time_boot_ms;                  // offset: 0
        map << flags;                         // offset: 4
    }

    inline void deserialize(mavlink::MsgMap &map) override
    {
        map >> time_boot_ms;                  // offset: 0
        map >> flags;                         // offset: 4
    }
};

} // namespace msg
} // namespace common
} // namespace mavlink
