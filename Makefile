
CC = g++
HEADERS = ../Headers
MAVLink = ../MAVLink

PIGPIO_PATH := $(PIGPIO_PATH)

LIB = -L$(PIGPIO_PATH)
INCLUDES = -I .. -I$(PIGPIO_PATH) 


all:
	$(CC) $(INCLUDES) $(LIB)  Driver.cpp -o robot_arm -lrt -lpthread -lwiringPi -lpigpio -lpigpiod_if2 || $(MAKE) pigpio pigpiod_if2

clean:
	rm robot_arm

pigpio:
	@echo ""
	@echo "Note that pigpio library is required."
	@echo "Warning! Use with caution. Consider to remove after trying this example out."
	@echo "To install:"
	@echo "sudo apt-get update"
